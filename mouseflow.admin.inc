<?php

/**
 *      @file
 *      mouseflow.admin.inc
 *      this file will be delete and it's code add to the original module.
 *
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

/**
 * @file
 * Admini form for mouseflow module
 */
function mouseflow_admin_settings(){
  $form = [];

  $form['mouseflow_code'] = array(
    '#title' => t('Mouseflow code'),
    '#description' =>  t('Copy and paste the mouseflow javascript code here:'),
    '#type' => 'textarea',
    '#default_value' => variable_get('mouseflow_code', NULL)
  );

  $form['mouseflow_admin_section'] = array(
    '#title' => t('Enable mouseflow on admin pages.'),
    '#description' => t('The mouseflow script will not be added to administrative sections of the site unless this option is enabled.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('mouseflow_admin_section', FALSE),
  );

  $form['mouseflow_blocked_ips'] = array(
    '#title' => t('Ip addresses'),
    '#description' => t('Please, add IP addresses for which you do not want to track, one path by line.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('mouseflow_blocked_ips', NULL)
  );

  $form['mouseflow_blocked_urls'] = array(
    '#title' => t('Urls'),
    '#description' => t('Please, add urls for wich you do not want to track, one path by line'),
    '#type' => 'textarea',
    '#default_value' => variable_get('mouseflow_blocked_urls', NULL),
  );


  return system_settings_form($form);
}
